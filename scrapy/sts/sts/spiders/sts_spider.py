import scrapy

# headers = {
#     'Connection': 'keep-alive',
#     'Cache-Control': 'max-age=0',
#     'DNT': '1',
#     'Upgrade-Insecure-Requests': '1',
#     'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36',
#     'Sec-Fetch-User': '?1',
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
#     'Sec-Fetch-Site': 'same-origin',
#     'Sec-Fetch-Mode': 'navigate',
#     'Accept-Encoding': 'gzip, deflate, br',
#     'Accept-Language': 'en-US,en;q=0.9',
# }

class PostsSpider(scrapy.Spider):
    name = "posts"
    # start_urls = [
    #     'https://www.katom.com/vendor/Univex.html',
    #     'https://www.katom.com/vendor/Southbend.html',
    #     'https://www.katom.com/vendor/Grindmaster.html',
    #     'https://www.katom.com/vendor/Howard McCray.html',
    #     'https://www.katom.com/vendor/Beverage Air.html',
    #     'https://www.katom.com/vendor/Atosa.html',
    #     'https://www.katom.com/vendor/Cookrite.html',
    #     'https://www.katom.com/vendor/Prepall.html',
    #     'https://www.katom.com/vendor/Mixrite.html',
    #     'https://www.katom.com/vendor/Eagle.html',
    #     'https://www.katom.com/vendor/Lainox.html',
    #     'https://www.katom.com/vendor/Rational.html',
    #     'https://www.katom.com/vendor/Moffat.html',
    #     'https://www.katom.com/vendor/Blodgett.html',
    #     'https://www.katom.com/vendor/Alto-Shaam.html',
    #     'https://www.katom.com/vendor/US Range.html',
    #     'https://www.katom.com/vendor/TRUE.html',
    #     'https://www.katom.com/vendor/American Range.html',
    #     'https://www.katom.com/vendor/Ice-O-Matic.html',
    #     'https://www.katom.com/vendor/Scotsman.html',
    #     'https://www.katom.com/vendor/Garland.html',
    #     'https://www.katom.com/vendor/Robot Coupe.html',
    #     'https://www.katom.com/vendor/Waring.html',
    #     'https://www.katom.com/vendor/Nemco.html',
    #     'https://www.katom.com/vendor/Winco.html',
    #     'https://www.katom.com/vendor/Advance Tabco.html',
    #     'https://www.katom.com/vendor/Krowne.html'   
    # ]
    start_urls = [
        'https://blog.scrapinghub.com/page/1',
        'https://blog.scrapinghub.com/page/2'
    ]    
    # start_urls = [
    #     'https://www.katom.com/vendor/Univex.html',
    #     'https://www.katom.com/vendor/Southbend.html',
    # ]


    # yield scrapy.Request('https://www.katom.com/vendor/Univex.html', headers=headers)

    def parse(self, response):
        page = response.url.split('/')[-1]
        filename = 'posts-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body.css('.read-more a').get())