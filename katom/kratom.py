from selenium import webdriver
from bs4 import BeautifulSoup
from lxml import html
import re
import requests
import csv
import time
import math

search_urls = ['https://www.katom.com/search?w=Univex&filter:brand=Univex&per_page=100&page=', 'https://www.katom.com/search?w=Southbend&filter:brand=Southbend&per_page=100&page=', 'https://www.katom.com/search?w=Grindmaster&filter:brand=Grindmaster&per_page=100&page=', 'https://www.katom.com/search?w=Howard McCray&filter:brand=Howard McCray&per_page=100&page=', 'https://www.katom.com/search?w=Beverage Air&filter:brand=Beverage Air&per_page=100&page=', 'https://www.katom.com/search?w=Atosa&filter:brand=Atosa&per_page=100&page=', 'https://www.katom.com/search?w=Cookrite&filter:brand=Cookrite&per_page=100&page=', 'https://www.katom.com/search?w=Prepall&filter:brand=Prepall&per_page=100&page=', 'https://www.katom.com/search?w=Mixrite&filter:brand=Mixrite&per_page=100&page=', 'https://www.katom.com/search?w=Eagle&filter:brand=Eagle&per_page=100&page=', 'https://www.katom.com/search?w=Lainox&filter:brand=Lainox&per_page=100&page=', 'https://www.katom.com/search?w=Rational&filter:brand=Rational&per_page=100&page=', 'https://www.katom.com/search?w=Moffat&filter:brand=Moffat&per_page=100&page=', 'https://www.katom.com/search?w=Blodgett&filter:brand=Blodgett&per_page=100&page=', 'https://www.katom.com/search?w=Alto-Shaam&filter:brand=Alto-Shaam&per_page=100&page=', 'https://www.katom.com/search?w=US Range&filter:brand=US Range&per_page=100&page=', 'https://www.katom.com/search?w=TRUE&filter:brand=TRUE&per_page=100&page=', 'https://www.katom.com/search?w=American Range&filter:brand=American Range&per_page=100&page=', 'https://www.katom.com/search?w=Ice-O-Matic&filter:brand=Ice-O-Matic&per_page=100&page=', 'https://www.katom.com/search?w=Scotsman&filter:brand=Scotsman&per_page=100&page=', 'https://www.katom.com/search?w=Garland&filter:brand=Garland&per_page=100&page=', 'https://www.katom.com/search?w=Robot Coupe&filter:brand=Robot Coupe&per_page=100&page=', 'https://www.katom.com/search?w=Waring&filter:brand=Waring&per_page=100&page=', 'https://www.katom.com/search?w=Nemco&filter:brand=Nemco&per_page=100&page=', 'https://www.katom.com/search?w=Winco&filter:brand=Winco&per_page=100&page=', 'https://www.katom.com/search?w=Advance Tabco&filter:brand=Advance Tabco&per_page=100&page=', 'https://www.katom.com/search?w=Krowne&filter:brand=Krowne&per_page=100&page=']


csv_file = open(f'kratom_product_data.csv', 'w', encoding='utf-8', newline='')
csv_writer = csv.writer(csv_file)
csv_writer.writerow(['product_page_url', 'product_name', 'product_manufacturer', 'product_mpn_sku', 'product_price', 'product_shipping_info', 'product_description', 'product_video', 'product_specs', 'product_meta_description', 'product_pdf_url', 'product_image_links', 'product_breadcrumbs'])


# Loop through search_urls
for cat_i in range(0,len(search_urls)):
	master_url = f'{search_urls[cat_i]}'
	source = requests.get(master_url).text
	soup = BeautifulSoup(source, 'lxml')
	total_search_results = soup.find('span', class_='results').text.strip()
	total_search_results = re.sub("[^0-9]", "", total_search_results)
	total_pages = math.ceil(int(total_search_results) / 100) + 1
	# Loop through all pages in search query
	for page in range(1,total_pages):
		url_pages = f'{master_url}{page}'
		source_page = requests.get(url_pages).text
		soup_page = BeautifulSoup(source_page, 'lxml')
		product_urls = soup_page.find('div', class_='products')
		# grab Product Page URLS (href)
		for link in product_urls.findAll('a'):
			product_page_url = link.get('href')
			source_product = requests.get(product_page_url).text
			soup_product = BeautifulSoup(source_product, 'lxml')
			print('Product Page URL: ' + product_page_url)
			# grab all Product Data
			product_name = soup_product.find('h1', class_='product-name').text.strip()
			product_mpn_sku = soup_product.find('span', class_='code').text.strip()
			product_price = soup_product.find('header', class_='price-display').text.strip()
			product_shipping_info = soup_product.find('p', class_='days').text.strip()
			product_meta_description = soup_product.find('meta', {'name': 'description'})['content']
			try:
				product_description = soup_product.find('div', class_='col-12 col-lg-8')
				product_description = str(product_description).strip()
				product_description = product_description.strip('\n')
				product_description = product_description.strip('\t')
				# print(product_description)
				# print('yep!!')
			except:
				product_description = soup_product.find('div', class_='col-12 col-lg-8')
				# print(product_description)
				# print('nope!!')
			product_video = soup_product.find('div', class_='mt-4')
			product_specs = soup_product.find('div', class_='col-12 col-lg-4')
			try:
				product_specs = product_specs.find('table').strip()
			except:
				product_specs = product_specs.find('table')
			product_manufacturer = soup_product.find('table', class_='specs-table').find('tbody').find('tr').find_all('td')[-1].text
			# product_manufacturer2 = soup_product.find('table', class_='specs-table').find('tbody').find_all('td').join(' ')
			print(product_manufacturer)
			# print(product_manufacturer2)
			pdf_container = soup_product.find('div', class_='spec-white-container')
			try:
				for pdf in pdf_container.findAll('a'):
					product_pdf_url = pdf.get('href')
			except:
				product_pdf_url = ''
			product_breadcrumbs = soup_product.find('ol', class_='breadcrumb').text.strip()
			
			product_images = soup_product.find('ul', class_='slides')
			for image in product_images.findAll('a'):
				product_image_links = image.get('rel')
				product_image_links = product_image_links[-1]
				product_image_links = "".join(product_image_links)
				csv_writer.writerow([product_page_url, product_name, product_manufacturer, product_mpn_sku, product_price, product_shipping_info, product_description, product_video, product_specs, product_meta_description, product_pdf_url, product_image_links, product_breadcrumbs])
csv_file.close()